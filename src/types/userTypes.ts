export interface IRegiter {
  email: string;
  password: string;
  roleId: number | '';
}

export interface ILogin {
  email: string;
  password: string;
}

export interface IAuth {
  roleId: number;
  userEmail: string;
  userId: number;
}

export interface IAuthState extends IAuth {
  userEmail: string;
  userId: number;
  roleId: number;
  loggedIn: boolean;
  authAsync: () => void;
}
