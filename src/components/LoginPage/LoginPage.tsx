import { FC, useContext } from 'react';
import {
  Box,
  Button,
  Container,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
  Stack,
} from '@chakra-ui/react';
import Link from 'next/link';
import { Formik, Field } from 'formik';
import * as yup from 'yup';
import { ILogin } from '@/types/userTypes';
import { UserService } from '@/services/userService/UserService';
import { AuthContext } from '@/layouts/AuthLayout';
import { useRouter } from 'next/router';

const initValues: ILogin = {
  email: '',
  password: '',
};

const validationSchema = yup.object().shape({
  email: yup.string().email('Неверный формат почты').required('Почта не может быть пустой'),
  password: yup
    .string()
    .required('Пароль не может быть пустым')
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
      'Пароль должен содержать как минимум 8 символов, одну заглавную букву, одну строчную букву, одну цифру и один спецсимвол'
    ),
});

const LoginPage: FC = () => {
  const router = useRouter();
  const AuthState = useContext(AuthContext);

  const onSubmit = async (values: ILogin) => {
    try {
      await UserService.Login({
        email: values.email,
        password: values.password,
      });
      await AuthState.authAsync();
      router.push('/');
    } catch (e) {}
  };

  return (
    <Container>
      <Flex height="100%" justifyContent="center" alignItems="center">
        <Box p="20px" bg="brandBlack" maxWidth="500px" width="100%" rounded={'md'}>
          <Heading mb="20px">Вход</Heading>
          <Formik
            initialValues={initValues}
            enableReinitialize
            validationSchema={validationSchema}
            onSubmit={values => {
              onSubmit(values);
            }}
          >
            {({ handleSubmit, errors, touched }) => (
              <form onSubmit={handleSubmit} style={{ width: '100%' }}>
                <Stack spacing="10px" width="100%">
                  <FormControl id="email" isInvalid={!!errors.email && touched.email}>
                    <FormLabel>E-mail</FormLabel>
                    <Field as={Input} id="email" name="email" type="email" />
                    <FormErrorMessage>{errors.email}</FormErrorMessage>
                  </FormControl>
                  <FormControl id="password" isInvalid={!!errors.password && touched.password}>
                    <FormLabel>Пароль</FormLabel>
                    <Field as={Input} id="password" name="password" type="password" />
                    <FormErrorMessage>{errors.password}</FormErrorMessage>
                  </FormControl>
                </Stack>
                <Flex justifyContent="space-between" mt="20px">
                  <Button type="submit" disabled={!!errors.email || !!errors.password}>
                    Войти
                  </Button>
                  <Link href="/register">
                    <Button variant="secondary">Зарегистрироваться</Button>
                  </Link>
                </Flex>
              </form>
            )}
          </Formik>
        </Box>
      </Flex>
    </Container>
  );
};

export default LoginPage;
