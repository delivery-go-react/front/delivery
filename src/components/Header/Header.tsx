import { AuthContext } from '@/layouts/AuthLayout';
import { LocationContext } from '@/layouts/LocationLayout';
import { UserService } from '@/services/userService/UserService';
import {
  Avatar,
  Box,
  Container,
  Flex,
  Grid,
  GridItem,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  useDisclosure,
  Icon,
} from '@chakra-ui/react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { FC, useContext } from 'react';
import { BsSearch } from 'react-icons/bs';
import { GoLocation } from 'react-icons/go';

const Header: FC = () => {
  const router = useRouter();
  const { userEmail } = useContext(AuthContext);
  const { defineLocation } = useContext(LocationContext);
  const { isOpen, onToggle } = useDisclosure();

  return (
    <Box>
      <Box
        position="fixed"
        zIndex="999"
        width="100%"
        height={{ base: isOpen ? '100%' : 'auto', md: 'auto' }}
        bg="brandBlack"
        py="20px"
      >
        <Container>
          <Grid templateAreas={`"Logo Search Search Search User"`} gap={6}>
            <GridItem w="100%" area={'Logo'}>
              <Flex justifyContent={'flex-start'} alignItems="center" height="100%">
                <Link href="/">
                  <Heading as="h1">Delivery</Heading>
                </Link>
              </Flex>
            </GridItem>
            <GridItem w="100%" area={'Search'}>
              <Flex alignItems="center" height="100%">
                <InputGroup>
                  <InputLeftElement pointerEvents="none">
                    <Icon as={BsSearch} />
                  </InputLeftElement>
                  <Input placeholder="Искать" />
                  <InputRightElement cursor="pointer" onClick={defineLocation}>
                    <Icon as={GoLocation} />
                  </InputRightElement>
                </InputGroup>
              </Flex>
            </GridItem>
            <GridItem w="100%" area="User">
              <Flex justifyContent={'flex-end'} alignItems="center" height="100%">
                <Menu>
                  <MenuButton>
                    <Avatar name={userEmail || ''} />
                  </MenuButton>
                  <MenuList bg="brandBlack" borderColor="brandSecondary">
                    <MenuItem
                      bg="brandBlack"
                      transition="0.25s"
                      _hover={{
                        bg: 'brandSecondary',
                      }}
                      onClick={async () => {
                        try {
                          await UserService.Logout();
                          router.push('/login');
                        } catch (e) {}
                      }}
                    >
                      Выйти
                    </MenuItem>
                  </MenuList>
                </Menu>
              </Flex>
            </GridItem>
          </Grid>
        </Container>
      </Box>
    </Box>
  );
};

export default Header;
