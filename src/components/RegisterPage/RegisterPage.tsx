import { FC } from 'react';
import {
  Box,
  Button,
  Container,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
  Select,
  Stack,
} from '@chakra-ui/react';
import Link from 'next/link';
import { Formik, Field } from 'formik';
import * as yup from 'yup';
import { IRole } from '@/types/roleTypes';
import { UserService } from '@/services/userService/UserService';
import { IRegiter } from '@/types/userTypes';

const initValues: IRegiter = {
  email: '',
  password: '',
  roleId: '',
};

const validationSchema = yup.object().shape({
  email: yup.string().email('Неверный формат почты').required('Почта не может быть пустой'),
  password: yup
    .string()
    .required('Пароль не может быть пустым')
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
      'Пароль должен содержать как минимум 8 символов, одну заглавную букву, одну строчную букву, одну цифру и один спецсимвол'
    ),
  roleId: yup.number().required('Тип аккаунта обязателен для выбора'),
});

const RegisterPage: FC<{ roles: IRole[] }> = ({ roles }) => {
  const onSubmit = async (values: IRegiter) => {
    try {
      const res = await UserService.Register({
        email: values.email,
        password: values.password,
        roleId: +values.roleId,
      });
    } catch (e) {}
  };

  return (
    <Container>
      <Flex height="100%" justifyContent="center" alignItems="center">
        <Box p="20px" bg="brandBlack" maxWidth="500px" width="100%" rounded={'md'}>
          <Heading mb="20px">Регистрация</Heading>
          <Formik
            initialValues={initValues}
            enableReinitialize
            validationSchema={validationSchema}
            onSubmit={values => {
              onSubmit(values);
            }}
          >
            {({ handleSubmit, errors, touched }) => (
              <form onSubmit={handleSubmit} style={{ width: '100%' }}>
                <Stack spacing="10px" width="100%">
                  <FormControl isRequired id="email" isInvalid={!!errors.email && touched.email}>
                    <FormLabel>E-mail</FormLabel>
                    <Field as={Input} id="email" name="email" type="email" />
                    <FormErrorMessage>{errors.email}</FormErrorMessage>
                  </FormControl>
                  <FormControl isRequired id="roleId" isInvalid={!!errors.roleId && touched.roleId}>
                    <FormLabel>Тип аккаунта</FormLabel>
                    <Field as={Select} id="roleId" name="roleId">
                      <option value="">Выберите тип пользователя</option>
                      {roles.map((role: IRole) => (
                        <option value={role.id} key={role.id}>
                          {role.label}
                        </option>
                      ))}
                    </Field>
                    <FormErrorMessage>{errors.roleId}</FormErrorMessage>
                  </FormControl>
                  <FormControl
                    isRequired
                    id="password"
                    isInvalid={!!errors.password && touched.password}
                  >
                    <FormLabel>Пароль</FormLabel>
                    <Field as={Input} id="password" name="password" type="password" />
                    <FormErrorMessage>{errors.password}</FormErrorMessage>
                  </FormControl>
                </Stack>
                <Flex justifyContent="space-between" mt="20px">
                  <Button type="submit" disabled={!!errors.email || !!errors.password}>
                    Регистрация
                  </Button>
                  <Link href="/login">
                    <Button variant="secondary">Вход</Button>
                  </Link>
                </Flex>
              </form>
            )}
          </Formik>
        </Box>
      </Flex>
    </Container>
  );
};

export default RegisterPage;
