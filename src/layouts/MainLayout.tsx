import { Flex, Box } from '@chakra-ui/react';
import { FC, PropsWithChildren } from 'react';
import Header from '@/components/Header/Header';
import Footer from '@/components/Footer/Footer';

const MainLayout: FC<PropsWithChildren<unknown>> = ({ children }) => {
  return (
    <Flex w="100%" direction="column" minHeight="100%" height="100%" position="relative">
      <Header />
      <Box flex="1 1 auto">{children}</Box>
      <Footer />
    </Flex>
  );
};

export default MainLayout;
