import { Box, Spinner, Flex } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { createContext, FunctionComponent, PropsWithChildren, useEffect, useState } from 'react';

const defaultLoader = {
  isLoading: false,
  toggleLoading: (val: boolean) => {},
};

export const LoaderContext = createContext(defaultLoader);

const LoaderLayout: FunctionComponent<PropsWithChildren> = ({ children }) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const toggleLoading = (val: boolean) => {
    setIsLoading(() => val);
  };
  const router = useRouter();

  useEffect(() => {
    const handleRouteChangeStart = () => {
      toggleLoading(true);
    };

    const handleRouteChangeComplete = () => {
      toggleLoading(false);
    };

    const handleRouteChangeError = () => {
      toggleLoading(false);
    };

    router.events.on('routeChangeStart', handleRouteChangeStart);
    router.events.on('routeChangeComplete', handleRouteChangeComplete);
    router.events.on('routeChangeError', handleRouteChangeError);

    return () => {
      router.events.off('routeChangeStart', handleRouteChangeStart);
      router.events.off('routeChangeComplete', handleRouteChangeComplete);
      router.events.off('routeChangeError', handleRouteChangeError);
    };
  }, [router.events]);

  return (
    <LoaderContext.Provider value={{ isLoading, toggleLoading }}>
      <Box w="100%" minHeight="100%" height="100%" position="relative">
        <>{children}</>
        {isLoading ? (
          <Flex
            position="fixed"
            top="0"
            left="0"
            bottom="0"
            right="0"
            w="100%"
            h="100%"
            justifyContent="center"
            alignItems="center"
            bg="blackAlpha.500"
            zIndex="999"
          >
            <Spinner thickness="4px" speed="0.65s" emptyColor="gray.200" color="blue.500" size="xl" />
          </Flex>
        ) : null}
      </Box>
    </LoaderContext.Provider>
  );
};

export default LoaderLayout;
