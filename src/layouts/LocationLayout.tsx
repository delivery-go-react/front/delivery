import { Box } from '@chakra-ui/react';
import { createContext, FC, PropsWithChildren, useCallback, useState } from 'react';

const defaultLocationState = {
  coordinates: { lat: 0, lng: 0 },
  defineLocation: () => {},
};

export const LocationContext = createContext(defaultLocationState);

const defaultCenter = {
  lat: 40.7128,
  lng: -74.006,
};

const LocationLayout: FC<PropsWithChildren<unknown>> = ({ children }) => {
  const [center, setCenter] = useState(defaultCenter);

  const defineLocation = useCallback(() => {
    navigator.geolocation.getCurrentPosition(
      position => {
        const { latitude, longitude } = position.coords;
        setCenter({ lat: latitude, lng: longitude });
      },
      error => {
        console.error('Error getting user location:', error);
      }
    );
  }, []);

  return (
    <LocationContext.Provider value={{ coordinates: center, defineLocation }}>
      <Box height="100%">{children}</Box>
    </LocationContext.Provider>
  );
};

export default LocationLayout;
