import { Box } from '@chakra-ui/react';
import { createContext, FC, PropsWithChildren, useEffect, useState } from 'react';
import { UserService } from '@/services/userService/UserService';
import { IAuthState } from '@/types/userTypes';
import { useRouter } from 'next/router';

const defaultAuthState: IAuthState = {
  userEmail: '',
  userId: 0,
  roleId: 0,
  loggedIn: false,
  authAsync: () => {},
};

export const AuthContext = createContext(defaultAuthState);

const notLoggedInPaths = ['/login', '/register'];

const AuthLayout: FC<PropsWithChildren<unknown>> = ({ children }) => {
  const [authState, setAuthState] = useState<IAuthState>(defaultAuthState);
  const router = useRouter();

  useEffect(() => {
    const AuthAsync = async () => {
      try {
        const data = await UserService.Auth();
        if (notLoggedInPaths.includes(router.asPath)) {
          router.push('/');
        }
        setAuthState({
          userEmail: data?.userEmail || '',
          userId: data?.userId || 0,
          roleId: data?.roleId || 0,
          loggedIn: data?.userId ? true : false,
          authAsync: () => {
            AuthAsync();
          },
        });
      } catch (e) {
        if (!notLoggedInPaths.includes(router.asPath)) {
          router.push('/login');
        }
        setAuthState(defaultAuthState);
      }
    };

    AuthAsync();
  }, [router]);

  return (
    <AuthContext.Provider value={authState}>
      <Box height="100%">{children}</Box>
    </AuthContext.Provider>
  );
};

export default AuthLayout;
