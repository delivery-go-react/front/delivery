import ContainerTheme from '@/theme/ContainerTheme';
import ButtonTheme from './ButtonTheme';

const Theme = {
  semanticTokens: {
    colors: {
      brandWhite: '#EEEEEE',
      brandBlue: '#00ADB5',
      brandSecondary: '#393E46',
      brandBlack: '#222831',
    },
  },
  styles: {
    global: {
      html: {
        minHeight: '100%',
        height: '100%',
        bg: 'brandSecondary',
      },
      body: {
        minHeight: '100%',
        height: '100%',
        bg: 'brandSecondary',
        color: 'brandWhite',
        fontSize: ['14px', null, '16px'],
        lineHeight: ['17px', null, '19px'],
        fontWeight: ['400', null, '400'],
      },
      '#__next': {
        height: '100%',
      },
      main: {
        height: '100%',
      },
      select: {
        option: {
          bg: 'brandSecondary !important',
        },
      },
    },
  },
  components: {
    Container: ContainerTheme,
    Button: ButtonTheme,
  },
};

export default Theme;
