const ButtonTheme = {
  baseStyle: {
    _focus: {
      boxShadow: 'none',
    },
  },
  variants: {
    primary: {
      bg: 'brandBlue',
      padding: '12px 20px',
      fontWeight: '600',
      height: 'auto',
      width: 'auto',
      color: 'brandWhite',
      borderRadius: '6px',
      _hover: {
        filter: 'brightness(85%)',
        _disabled: {
          bg: 'brandBlue',
          filter: 'brightness(85%)',
          cursor: 'not-allowed',
        },
      },
      _active: {
        bg: 'brandBlue',
      },
    },

    secondary: {
      bg: 'brandSecondary',
      padding: '12px 20px',
      fontWeight: '600',
      height: 'auto',
      width: 'auto',
      color: 'brandWhite',
      borderRadius: '6px',
      _hover: {
        filter: 'brightness(85%)',
        bg: 'brandSecondary',
        _disabled: {
          filter: 'brightness(85%)',
          bg: 'brandSecondary',
          cursor: 'not-allowed',
        },
      },
      _active: {
        bg: 'brandSecondary',
      },
    },

    outline: {
      bg: 'transparent',
      padding: '12px 20px',
      fontWeight: '600',
      height: 'auto',
      width: 'auto',
      color: 'blue.600',
      borderRadius: '6px',
      borderColor: 'blue.600',
      _hover: {
        bg: 'blue.700',
        color: 'white',
        _disabled: {
          bg: 'blue.700',
          cursor: 'not-allowed',
        },
      },
      _active: {
        bg: 'blue.700',
        color: 'white',
      },
    },
  },

  defaultProps: {
    variant: 'primary',
  },
};

export default ButtonTheme;
