const ContainerTheme = {
  baseStyle: {
    maxW: '1440px',
    pr: { base: '20px', lg: '40px', xl: '60px' },
    pl: { base: '20px', lg: '40px', xl: '60px' },
    height: '100%',
  },
};

export default ContainerTheme;
