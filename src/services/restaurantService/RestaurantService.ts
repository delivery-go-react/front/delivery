import { secured } from '@/services/axiosService';

export const RestaurantService = {
  async getAll() {
    try {
      const { data } = await secured.get('/restaurants');
      return data;
    } catch (e) {
      return Promise.reject(e);
    }
  },
};
