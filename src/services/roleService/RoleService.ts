import { plain, secured } from '@/services/axiosService';
import { IRole } from '@/types/roleTypes';

export const RoleService = {
  async getAll() {
    try {
      const { data } = await plain.get<{ roles: IRole[] }>('/role/get-roles');
      return data;
    } catch (e) {
      return Promise.reject(e);
    }
  },

  async getPermissions() {
    try {
      const { data } = await secured.get('/role/get-permissions');
    } catch (e) {
      return Promise.reject(e);
    }
  },
};
