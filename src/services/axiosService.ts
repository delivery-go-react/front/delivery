import { LoaderContext } from '@/layouts/LodaerLayout';
import { useToast } from '@chakra-ui/react';
import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { FC, ReactElement, ReactNode, useContext, useEffect } from 'react';

export const baseURL = process.env.API_URL;

export const plain: AxiosInstance = axios.create({
  baseURL,
  withCredentials: false,
  headers: {
    'Content-Type': 'application/json',
  },
});

export const secured: AxiosInstance = axios.create({
  baseURL,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json',
  },
});

interface AxiosInterceptorProps {
  children: ReactElement;
}

export const AxiosInterceptor: FC<AxiosInterceptorProps> = ({ children }): ReactElement => {
  const { toggleLoading } = useContext(LoaderContext);
  const toast = useToast();

  useEffect(() => {
    const reqInterceptor = (request: any) => {
      toggleLoading(true);
      return request;
    };

    const resInterceptor = (response: AxiosResponse) => {
      toggleLoading(false);
      return response;
    };

    const errInterceptor = (
      error: AxiosError<{
        error: string;
      }>
    ) => {
      try {
        toggleLoading(false);
        if (!error?.request.responseURL.includes('without_error=true')) {
          if (error?.request?.status >= 500) {
            toast({
              title: `Ошибка сервера`,
              description: 'Что-то пошло не так, обратитесь в службу поддержки',
              status: 'error',
              isClosable: true,
              position: 'top-right',
              duration: 5000,
            });

            return Promise.reject(error);
          }

          if (error?.response?.data?.error) {
            toast({
              title: `Ошибка`,
              description: error.response.data.error,
              status: 'error',
              isClosable: true,
              position: 'top-right',
              duration: 5000,
            });

            return Promise.reject(error);
          }

          if (error.message) {
            toast({
              title: `Ошибка`,
              description: error.message,
              status: 'error',
              isClosable: true,
              position: 'top-right',
              duration: 5000,
            });

            return Promise.reject(error);
          }
        }
        return Promise.reject(error);
      } catch (e) {}
    };

    plain.interceptors.request.use(reqInterceptor, errInterceptor);
    secured.interceptors.request.use(reqInterceptor, errInterceptor);

    const interceptorPlain = plain.interceptors.response.use(resInterceptor, errInterceptor);
    const interceptorSecured = secured.interceptors.response.use(resInterceptor, errInterceptor);

    return () => {
      plain.interceptors.response.eject(interceptorPlain);
      secured.interceptors.response.eject(interceptorSecured);
    };
  }, []);

  return children;
};
