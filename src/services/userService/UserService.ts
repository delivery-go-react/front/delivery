import { plain, secured } from '@/services/axiosService';
import { IAuth, ILogin, IRegiter } from '@/types/userTypes';

export const UserService = {
  async Register(data: IRegiter) {
    try {
      const res = await plain.post('/user/register', {
        email: data.email,
        password: data.password,
        roleId: data.roleId,
      });
      return res;
    } catch (e) {
      return Promise.reject(e);
    }
  },

  async Login(data: ILogin) {
    try {
      const res = await secured.post('/user/login', {
        email: data.email,
        password: data.password,
      });
      return res;
    } catch (e) {
      return Promise.reject(e);
    }
  },

  async Auth() {
    try {
      const { data } = await secured.get<IAuth>('/user/auth', {
        params: {
          without_error: true,
        },
      });
      return data;
    } catch (e) {
      return Promise.reject(e);
    }
  },

  async Logout() {
    try {
      const res = await secured.post('/user/logout');
      return res;
    } catch (e) {
      return Promise.reject(e);
    }
  },
};
