import RegisterPage from '@/components/RegisterPage/RegisterPage';
import { RoleService } from '@/services/roleService/RoleService';
import { IRole } from '@/types/roleTypes';
import { NextPage } from 'next';
import Head from 'next/head';

const Register: NextPage<{ roles: IRole[] }> = ({ roles }) => {
  return (
    <>
      <Head>
        <title>Delivery | Register</title>
      </Head>
      <main>
        <RegisterPage roles={roles} />
      </main>
    </>
  );
};

export async function getStaticProps() {
  let data: { roles: IRole[] } = { roles: [] };

  try {
    data = await RoleService.getAll();
  } catch (e) {}

  return {
    props: {
      roles: data?.roles.filter((role: IRole) => role.name !== 'admin') || null,
    },
    revalidate: 60 * 5,
  };
}

export default Register;
