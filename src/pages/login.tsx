import LoginPage from '@/components/LoginPage/LoginPage';
import Head from 'next/head';

const Login = () => {
  return (
    <>
      <Head>
        <title>Delivery | Login</title>
      </Head>
      <main>
        <LoginPage />
      </main>
    </>
  );
};

export default Login;
