import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import type { AppProps } from 'next/app';
import AuthLayout from '@/layouts/AuthLayout';
import LocationLayout from '@/layouts/LocationLayout';
import Theme from '@/theme';
import LoaderLayout from '@/layouts/LodaerLayout';
import { AxiosInterceptor } from '@/services/axiosService';

const App = ({ Component, pageProps }: AppProps) => {
  const theme = extendTheme(Theme);

  return (
    <ChakraProvider theme={theme}>
      <LoaderLayout>
        <AuthLayout>
          <LocationLayout>
            <AxiosInterceptor>
              <Component {...pageProps} />
            </AxiosInterceptor>
          </LocationLayout>
        </AuthLayout>
      </LoaderLayout>
    </ChakraProvider>
  );
};

export default App;
